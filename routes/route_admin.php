<?php

    Route::group(['prefix' => 'admin', 'namespace' => 'Admin\Auth'], function () {

        Route::get('login','AdminLoginController@loginAdmin')->name('get.admin.login');

        Route::post('login','AdminLoginController@postLoginAdmin');

        Route::get('logout','AdminLoginController@logoutAdmin')->name('get.admin.logout');
    });

    Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'middleware' => 'check_login_admin'], function () {

        Route::get('', 'AdminController@index')->name('admin.index');

        /* Route danh mục sản phẩm */
        Route::group(['prefix' => 'category'], function () {

            Route::get('','AdminCategoryController@index')->name('admin.category.index');

            Route::get('create', 'AdminCategoryController@create')->name('admin.category.create');
            Route::post('create', 'AdminCategoryController@store');

            Route::get('update/{id}', 'AdminCategoryController@edit')->name('admin.category.update');
            Route::post('update/{id}', 'AdminCategoryController@update');

            Route::get('active/{id}', 'AdminCategoryController@active')->name('admin.category.active');
            Route::get('hot/{id}', 'AdminCategoryController@hot')->name('admin.category.hot');

            Route::get('delete/{id}', 'AdminCategoryController@delete')->name('admin.category.delete');

        });

        /* Route danh mục keyword */
        Route::group(['prefix' => 'keyword'], function () {

            Route::get('','AdminKeywordController@index')->name('admin.keyword.index');

            Route::get('create', 'AdminKeywordController@create')->name('admin.keyword.create');
            Route::post('create', 'AdminKeywordController@store');

            Route::get('update/{id}', 'AdminKeywordController@edit')->name('admin.keyword.update');
            Route::post('update/{id}', 'AdminKeywordController@update');

            Route::get('active/{id}', 'AdminKeywordController@active')->name('admin.keyword.active');
            Route::get('hot/{id}', 'AdminKeywordController@hot')->name('admin.keyword.hot');

            Route::get('delete/{id}', 'AdminKeywordController@delete')->name('admin.keyword.delete');

        });

        /* Route danh mục product */
        Route::group(['prefix' => 'product'], function () {

            Route::get('','AdminProductController@index')->name('admin.product.index');

            Route::get('create', 'AdminProductController@create')->name('admin.product.create');
            Route::post('create', 'AdminProductController@store');

            Route::get('update/{id}', 'AdminProductController@edit')->name('admin.product.update');
            Route::post('update/{id}', 'AdminProductController@update');

            Route::get('active/{id}', 'AdminProductController@active')->name('admin.product.active');
            Route::get('hot/{id}', 'AdminProductController@hot')->name('admin.product.hot');

            Route::get('delete/{id}', 'AdminProductController@delete')->name('admin.product.delete');

        });

        /* Route danh mục attribute */
        Route::group(['prefix' => 'attribute'], function () {

            Route::get('','AdminAttributeController@index')->name('admin.attribute.index');

            Route::get('create', 'AdminAttributeController@create')->name('admin.attribute.create');
            Route::post('create', 'AdminAttributeController@store');

            Route::get('update/{id}', 'AdminAttributeController@edit')->name('admin.attribute.update');
            Route::post('update/{id}', 'AdminAttributeController@update');

            Route::get('delete/{id}', 'AdminAttributeController@delete')->name('admin.attribute.delete');

            Route::get('active/{id}', 'AdminAttributeController@active')->name('admin.attribute.active');
            Route::get('hot/{id}', 'AdminAttributeController@hot')->name('admin.attribute.hot');

        });

        /* Route user */
        Route::group(['prefix' => 'user'], function () {

            Route::get('','AdminUserController@index')->name('admin.user.index');


            Route::get('update/{id}', 'AdminUserController@edit')->name('admin.user.update');
            Route::post('update/{id}', 'AdminUserController@update');


            Route::get('delete/{id}', 'AdminUserController@delete')->name('admin.user.delete');

        });

        /* Route transaction */
        Route::group(['prefix' => 'transaction'], function () {

            Route::get('','AdminTransactionController@index')->name('admin.transaction.index');

            Route::get('update/{id}', 'AdminTransactionController@edit')->name('admin.transaction.update');
            Route::post('update/{id}', 'AdminTransactionController@update');

            Route::get('delete/{id}', 'AdminTransactionController@delete')->name('admin.transaction.delete');
            Route::get('order-delete/{id}', 'AdminTransactionController@deleteOrderItem')->name('ajax.admin.transaction.delete');

            Route::get('view-transaction/{id}', 'AdminTransactionController@getTransactionDetail')->name('ajax.admin.transaction.detail');

            Route::get('action/{action}/{id}','AdminTransactionController@getAction')->name('admin.action.transaction');

        });

        /* Route danh mục menu */
        Route::group(['prefix' => 'menu'], function () {

            Route::get('','AdminMenuController@index')->name('admin.menu.index');

            Route::get('create', 'AdminMenuController@create')->name('admin.menu.create');
            Route::post('create', 'AdminMenuController@store');

            Route::get('update/{id}', 'AdminMenuController@edit')->name('admin.menu.update');
            Route::post('update/{id}', 'AdminMenuController@update');

            Route::get('delete/{id}', 'AdminMenuController@delete')->name('admin.menu.delete');

            Route::get('status/{id}', 'AdminMenuController@status')->name('admin.menu.status');
            Route::get('hot/{id}', 'AdminMenuController@hot')->name('admin.menu.hot');

        });

        /* Route danh mục menu */
        Route::group(['prefix' => 'article'], function () {

            Route::get('','AdminArticleController@index')->name('admin.article.index');

            Route::get('create', 'AdminArticleController@create')->name('admin.article.create');
            Route::post('create', 'AdminArticleController@store');

            Route::get('update/{id}', 'AdminArticleController@edit')->name('admin.article.update');
            Route::post('update/{id}', 'AdminArticleController@update');

            Route::get('delete/{id}', 'AdminArticleController@delete')->name('admin.article.delete');

            Route::get('active/{id}', 'AdminArticleController@active')->name('admin.article.active');
            Route::get('hot/{id}', 'AdminArticleController@hot')->name('admin.article.hot');

        });

        /* Route Slide menu */
        Route::group(['prefix' => 'slide'], function () {

            Route::get('','AdminSlideController@index')->name('admin.slide.index');

            Route::get('create', 'AdminSlideController@create')->name('admin.slide.create');
            Route::post('create', 'AdminSlideController@store');

            Route::get('update/{id}', 'AdminSlideController@edit')->name('admin.slide.update');
            Route::post('update/{id}', 'AdminSlideController@update');

            Route::get('delete/{id}', 'AdminSlideController@delete')->name('admin.slide.delete');

            Route::get('status/{id}', 'AdminSlideController@active')->name('admin.slide.active');
            Route::get('hot/{id}', 'AdminSlideController@hot')->name('admin.slide.hot');

        });

    });
