<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\AdminRequestArticle;
use App\Models\Article;
use App\Models\Menu;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class AdminArticleController extends Controller
{
    public function index()
    {
        $articles = Article::orderByDesc('id')->get();
        return view('admin.article.index', compact('articles'));
    }

    public function create()
    {
        $menus = Menu::all();
        $viewData = [
            'menus' => $menus
        ];
        return view('admin.article.create', $viewData);
    }

    public function store(AdminRequestArticle $request)
    {
        $data = $request->except('_token','a_avatar');
        $data['a_slug'] = Str::slug($request->a_name);
        $data['created_at'] = Carbon::now();

        if ($request->a_avatar) {
            $image = upload_image('a_avatar');
            if ($image['code'] == 1)
                $data['a_avatar'] = $image['name'];
        }

        $id = Article::insertGetId($data);

        \Session::flash('toastr', [
            'type' => 'success',
            'message' => 'Thêm bài viết thành công!'
        ]);

        return redirect()->route('admin.article.index');
    }

    public function active($id){
        $article           = Article::find($id);
        $article->a_active = ! $article->a_active;
        $article->save();

        \Session::flash('toastr', [
            'type' => 'success',
            'message' => 'Cập nhật thành công!'
        ]);

        return redirect()->back();
    }

    public function hot($id){
        $article           = Article::find($id);
        $article->a_hot    = ! $article->a_hot;
        $article->save();

        \Session::flash('toastr', [
            'type' => 'success',
            'message' => 'Cập nhật thành công!'
        ]);

        return redirect()->back();
    }

    public function edit($id)
    {
        $menus = Menu::all();
        $article = Article::find($id);
        $viewData = [
            'menus' => $menus,
            'article' => $article
        ];
        return view('admin.article.update', $viewData);
    }

    public function update(AdminRequestArticle $request, $id)
    {
        $article = Article::find($id);
        $data = $request->except('_token', 'a_avatar');
        $data['a_slug'] = Str::slug($request->a_name);
        $data['updated_at'] = Carbon::now();
        if ($request->a_avatar) {
            $image = upload_image('a_avatar');
            if ($image['code'] == 1)
                $data['a_avatar'] = $image['name'];
        }

        $update  = $article->update($data);

        \Session::flash('toastr', [
            'type' => 'success',
            'message' => 'Cập nhật thành công!'
        ]);

        return redirect()->route('admin.article.index');
    }

    public function delete($id)
    {
        $article = Article::find($id);
        if($article) $article->delete();

        \Session::flash('toastr', [
            'type' => 'success',
            'message' => 'Xoá bải viết thành công!'
        ]);

        return redirect()->back();
    }

}
