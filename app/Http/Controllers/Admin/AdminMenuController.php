<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\AdminMenuRequest;
use App\Models\Menu;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class AdminMenuController extends Controller
{
    public function index()
    {
        $menus = Menu::orderByDesc('id')->get();
        $viewData = [
            'menus' => $menus
        ];
        return view('admin.menu.index', $viewData);
    }

    public function create()
    {
        return view('admin.menu.create');
    }

    public function store(AdminMenuRequest $request)
    {
        $data               = $request->except('_token');
        $data['mn_slug']    = Str::slug($request->mn_name);
        $data['created_at'] = Carbon::now();
        $id                 = Menu::insertGetId($data);

        \Session::flash('toastr', [
            'type' => 'success',
            'message' => 'Thêm menu thành công!'
        ]);

        return redirect()->route('admin.menu.index');
    }

    public function edit($id)
    {
        $menu = Menu::find($id);
        return view('admin.menu.update', compact('menu'));
    }

    public function update(AdminMenuRequest $request, $id)
    {
        $menu = Menu::find($id);
        $data               = $request->except('_token');
        $data['mn_slug']     = Str::slug($request->mn_name);
        $data['updated_at'] = Carbon::now();

        $menu->update($data);
        \Session::flash('toastr', [
            'type' => 'success',
            'message' => 'Cập nhật menu thành công!'
        ]);
        return redirect()->route('admin.menu.index');
    }

    public function delete($id)
    {
        $menu = Menu::find($id);
        if($menu) $menu->delete();

        \Session::flash('toastr', [
            'type' => 'success',
            'message' => 'Xoá menu thành công!'
        ]);

        return redirect()->back();
    }

    public function hot($id)
    {
        $menu           = Menu::find($id);
        $menu->mn_hot = ! $menu->mn_hot;
        $menu->save();

        \Session::flash('toastr', [
            'type' => 'success',
            'message' => 'Cập nhật trạng thái thành công!'
        ]);

        return redirect()->back();
    }

    public function status($id)
    {
        $menu           = Menu::find($id);
        $menu->mn_status = ! $menu->mn_status;
        $menu->save();

        \Session::flash('toastr', [
            'type' => 'success',
            'message' => 'Cập nhật trạng thái thành công!'
        ]);

        return redirect()->back();
    }
}
