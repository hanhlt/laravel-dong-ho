<?php

namespace App\Http\Controllers\Admin\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

class AdminLoginController extends Controller
{
    use AuthenticatesUsers;

    public function loginAdmin()
    {
        return view('admin.auth.login');
    }
    public function postLoginAdmin(Request $request)
    {
        if(\Auth::guard('admins')->attempt(['email' => $request->email, 'password' => $request->password])){
            return redirect()->intended('/admin');
        }
        return redirect()->back();
    }

    public function logoutAdmin()
    {
        \Auth::guard('admins')->logout();
        return redirect()->to('/');
    }
}
