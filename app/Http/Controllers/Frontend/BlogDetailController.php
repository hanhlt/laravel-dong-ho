<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Article;
use Illuminate\Http\Request;

class BlogDetailController extends BlogBaseController
{
    public function getDetail(Request $request, $slug)
    {
        $arraySlug = explode('-', $slug);
        $id = array_pop($arraySlug);

        if($id)
        {
            $article = Article::find($id);
            $titleArticle = $article->a_name;

            $viewData = [
                'article' => $article,
                'title_page' => $titleArticle,
                'products' => $this->getProductTop()
            ];
        }
        return view('frontend.pages.blog_detail.index', $viewData);
    }
}
