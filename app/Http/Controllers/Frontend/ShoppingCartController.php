<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\Product;
use App\Models\Transaction;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ShoppingCartController extends Controller
{
    public function index()
    {
        $shopping =  \Cart::content();
        return view('frontend.pages.shopping.index', compact('shopping'));
    }
    public function add($id)
    {
        $product = Product::find($id);
        if(!$product) return redirect()->to('/');
        //kiểm tra số lượng sản phẩm

        if($product->pro_number <1 ){
            \Session::flash('toastr', [
                'type' => 'error',
                'message' => 'Số lượng sản phẩm không đủ!'
            ]);

            return redirect()->back();
        }

        \Cart::add([
            'id' => $product->id,
            'name' => $product->pro_name,
            'qty' => 1,
            'weight' => 1,
            'price' => number_price($product->pro_price, $product->pro_sale),
            'options' => [
                'sale' => $product->pro_sale,
                'price_old' =>$product->pro_price,
                'image' => $product->pro_avatar
            ]
        ]);

        \Session::flash('toastr', [
            'type' => 'success',
            'message' => 'Thêm sản phẩm thành công!'
        ]);

        return redirect()->back();
    }
    public function delete($rowId)
    {
        \Cart::remove($rowId);
        \Session::flash('toastr', [
            'type' => 'success',
            'message' => 'Xoá sản phẩm thành công!'
        ]);
        return redirect()->back();
    }

    public function postPay(Request $request)
    {
        $data = $request->except('_token');
        if(isset(Auth::user()->id)){
            $data['tst_user_id'] = Auth::user()->id;
        }
        $data['tst_total_money'] = str_replace(',', '', \Cart::subtotal(0));
        $data['created_at'] = Carbon::now();
        $transactionID = Transaction::insertGetId($data);
        if($transactionID){
            $shopping = \Cart::content();
            foreach ($shopping as $key => $item){

                //Lưu đơn hàng
                Order::insert([
                   'od_transaction_id' => $transactionID,
                   'od_product_id'     => $item->id,
                   'od_sale'           => $item->options->sale,
                   'od_qty'            => $item->qty,
                   'od_price'          => $item->price
                ]);

                //Tăng số lượng
                \DB::table('products')
                    ->where('id', $item->id)
                    ->increment("pro_pay");
            }
        }
        \Cart::destroy();
        \Session::flash('toastr', [
            'type' => 'success',
            'message' => 'Bạn đã đặt hàng thành công!'
        ]);
        return redirect()->to('/');
    }

    public function update(Request $request,$id)
    {
        if($request->ajax()){
            $qty = $request->qty ?? 1;
            $idProduct = $request->idProduct;
            $product = Product::find($idProduct);

            if(!$product) return response(['message' => 'Không tồn tại sản phẩm cần update']);

            if($product->pro_number < $qty){
                return response(['message' => 'Số lượng cập nhật không đủ']);
            }

            \Cart::update($id, $qty);
            return response(['message' => 'Cập nhật thành công']);
        }
    }
}
