<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Product;
use Illuminate\Http\Request;

class BlogBaseController extends Controller
{
    public function getProductTop()
    {
        $products = Product::where([
            'pro_active' => 1
        ])->where('pro_pay', '>',0)
            ->orderByDesc('pro_pay')
            ->limit(5)
            ->select('id','pro_name','pro_slug','pro_sale','pro_avatar','pro_price','pro_category_id')
            ->get();

        return $products;
    }
}
