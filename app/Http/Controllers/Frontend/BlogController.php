<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Article;
use Illuminate\Http\Request;

class BlogController extends BlogBaseController
{
    public function index()
    {
        $articles = Article::where([
            'a_active' => 1
        ])->select('id','a_name','a_slug','a_description','a_avatar')
            ->orderByDesc('id')
            ->paginate(10);

        $viewData = [
            'articles' => $articles,
            'products' => $this->getProductTop()
        ];
        return view('frontend.pages.blog.index', $viewData);
    }
}
