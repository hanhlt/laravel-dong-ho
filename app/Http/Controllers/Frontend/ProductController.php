<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Attribute;
use App\Models\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function index(Request $request)
    {
        $paramAtbSearch = $request->except('price', 'page', 'k', 'country');
        $paramAtbSearch = array_values($paramAtbSearch);

        $products = Product::where('pro_active', 1);

        if(!empty($paramAtbSearch)){
            $products->whereHas('attributes', function ($query) use ($paramAtbSearch){
               $query->whereIn('pa_attribute_id', $paramAtbSearch);
            });
        }

        if($name = $request->k) $products->where('pro_name', 'like', '%'.$name.'%');
        if($country = $request->country) $products->where('pro_country', $country);

        if($request->price)
        {
            $price = $request->price;
            if($price == 6){
                $products->where('pro_price', '>', 10000000);
            }else{
                $products->where('pro_price', '<=', $price * 2000000);
            }
        }

        $products = $products->orderByDesc('id')
            ->select('id','pro_name','pro_slug','pro_sale','pro_avatar','pro_price')
            ->paginate(10);

        $attributes = $this->syncAttributeGroup();

        $modelProduct = new Product();

        $viewData = [
           'attributes' => $attributes,
            'products' => $products,
            'query' => $request->query(),
            'country' => $modelProduct->country
        ];
        return view('frontend.pages.product.index', $viewData);
    }

    public function syncAttributeGroup()
    {
        $attributes = Attribute::get();
        $groupAttribute = [];
        foreach ($attributes as $key => $attribute)
        {
            $key = $attribute->gettype($attribute->atb_type)['name'];
            $groupAttribute[$key][] = $attribute->toArray();
        }
        return $groupAttribute;
    }
}
