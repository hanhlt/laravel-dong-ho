<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Service\ProcessViewService;
use Illuminate\Http\Request;

class ProductDetailController extends Controller
{
    public function getProductDetail(Request $request, $slug)
    {
        $arraySlug = explode('-', $slug);
        $id = array_pop($arraySlug);

        if($id)
        {
            $product = Product::with('category:id,c_name,c_slug', 'keywords')->findOrFail($id);
            $productSuggests = $this->getProductSuggests($product->pro_category_id);
            $titleProduct = $product->pro_name;

            $viewData = [
                'product' => $product,
                'productSuggests' => $productSuggests,
                'title_page' => $titleProduct
            ];
        }
        ProcessViewService::view('products','pro_view','product',$id);
        return view('frontend.pages.product_detail.index', $viewData);
    }

    public function getProductSuggests($categoryID)
    {
        $products = Product::where([
            'pro_active' => 1,
            'pro_category_id' => $categoryID
        ])
            ->orderByDesc('id')
            ->limit(12)
            ->select('id','pro_name','pro_slug','pro_avatar','pro_price','pro_sale')
            ->get();

        return $products;
    }
}
