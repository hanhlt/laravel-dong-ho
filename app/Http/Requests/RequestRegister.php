<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use function foo\func;

class RequestRegister extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|max:190|min:3|unique:users,email'.$this->id,
            'name' => 'required',
            'password' => 'required',
            'phone' => 'required|unique:users,phone,'.$this->id
        ];
    }
    public function messages()
    {
        return [
            'email.required' => 'Email không được để trống',
            'email.unique' => 'Email đã được đăng ký',
            'phone.unique' => 'Số điện thoại đã được đăng ký',
            'phone.required' => 'Số điện thoại không được để trống',
            'password.required' => 'Mật khẩu không được để trống',
            'name.required' => 'Tên không được để trống'
        ];
    }
}
