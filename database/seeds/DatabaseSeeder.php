<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class  DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        \DB::table('admins')->insert([
            'name' => 'La Thanh Hạnh',
            'email' => 'lathanhhanh@dz.dev',
            'phone' => '0369283980',
            'password' => Hash::make('123456')
        ]);
    }
}
