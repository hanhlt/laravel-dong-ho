@extends('layouts.app_master_frontend')
@section('content')
    <div id="container">
        <div class="wrp">
            <div id="product" class="detail">
                <div class="pathway">
                    <ul>
                        <li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a itemprop="url" href="{{ route('get.home') }}" title="Home"><span itemprop="title">Trang chủ</span></a> </li>
                        <li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a itemprop="url" href="" title="Đồng hồ chính hãng"><span itemprop="title">Đồng hồ chính hãng</span></a></li>
                        <li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a itemprop="url" href="#" title="Đồng hồ Philippe Auguste"><span itemprop="title">Đồng hồ Philippe Auguste</span></a></li>
                    </ul>
                </div>
                <div class="top">
                    <div class="left">
                        <div class="imgLarge">
                            <div class="wImage">
                                <a href="javascript://" title="" class="image">
                                    <img data-lazy="{{ pare_url_file($product->pro_avatar) }}" class="lazyload" alt="Dang Quang" src="{{ pare_url_file($product->pro_avatar) }}"/>
                                </a>
                            </div>
                        </div>
                        <div class="cb"></div>
                    </div>
                    <div class="right">
                        <h1 class="namePro">{{ $product->pro_name }}</h1>
                        <div class="flex">
                            <div class="rightl" style="margin-right: 20px;">
                                <div class="prices">
                                    <p>Giá niêm yết: <span class="value">{{ number_format($product->pro_price,0,",",".") }} đ</span></p>
                                    <p>
                                        @php
                                            $price = ((100 - $product->pro_sale) * $product->pro_price ) / 100;
                                        @endphp
                                        Giá bán: <span class="value priceNew">{{ number_format($price,0,',','.') }} đ</span>
                                        <span class="sale">-{{ $product->pro_sale }}%</span>

                                    </p>
                                    <p class="">
                                        <span>Lượt xem: </span>
                                        <span>{{ $product->pro_view }}</span>
                                    </p>
                                </div>
                                <div class="btnCart">
                                    <a href="{{ route('get.shopping.add', $product->id) }}" title=""  class="muangay">
                                        <span>Mua ngay</span>
                                        <span>Hotline: 1800.6005</span>
                                    </a>
                                    <a href="#" title="" onclick="add_cart_detail('17722',1);" class="muatragop">
                                        <span>Mua trả góp 0%</span>
                                        <span>Visa, Master, JCB</span>
                                    </a>
                                </div>
                                <div class="infomation">
                                    <h2 class="title">Thông số kỹ thuật</h2>
                                    <div class="group">
                                        <div class="item">
                                            <p class="text1">Danh mục:</p>
                                            <h3 class="text2">
                                                @if (isset($product->category->c_name))
                                                    <a href="{{ route('get.category.list', $product->category->c_slug).'-'.$product->pro_category_id }}">{{ $product->category->c_name }}</a>
                                                @endif
                                            </h3>
                                        </div>
                                        <div class="item">
                                            <p class="text1">Xuất xứ:</p>
                                            <h3 class="text2">{{ $product->getConutry($product->pro_country) }}</h3>
                                        </div>
                                        <div class="item">
                                            <p class="text1">Độ chịu nước:</p>
                                            <h2 class="text2">{{ $product->pro_resistant }}</h2>
                                        </div>
                                        <div class="item">
                                            <p class="text1">Năng lượng:</p>
                                            <h3 class="text2">{{ $product->pro_energy }}</h3>
                                        </div>
                                    </div>
                                </div>
                                <div class="infomation" style="margin-top: 20px;">
                                    <h2 class="title">Từ khoá</h2>
                                    <div class="group">
                                        @if(isset($product->keywords))
                                            <div class="item">
                                                @foreach($product->keywords as $keyword)
                                                    <a href="" style="padding: 7px; border: 1px solid blueviolet;border-radius: 5px;margin-right: 5px;font-weight: bold;font-size: 12px;color: blueviolet;">
                                                        {{ $keyword->k_name }}
                                                    </a>
                                                @endforeach
                                            </div>
                                        @endif

                                    </div>
                                </div>

                            </div>
                            <div class="rightr">
                                <a href="http://bit.ly/37wPRl4" title="Giam giá" target="_blank"><img alt="Hoan tien" style="width: 100%;padding-bottom: 10px;" src="https://www.dangquangwatch.vn/view/sanpham/sale142-2020.jpg"/></a>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="bot">
                    <div class="left">
                        <div class="tabs">
                            <div class="tab-content">
                                <div id="tab1" class="tab active">
                                    <div class="RelatedPro">
                                        <div class="group">
                                            @foreach($productSuggests as $product)
                                                <div class="item">
                                                    @include('frontend.components.product_item', ['product' => $product])
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="right">

                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
