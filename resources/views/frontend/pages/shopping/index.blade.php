@extends('layouts.app_master_frontend')
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
<style>
    .list .title{
        font-size: 18px;
        padding: 10px 0;
        font-weight: bold;
    }
    .customer .title{
        font-size: 18px;
        padding: 10px 0;
        font-weight: bold;
    }
</style>
@section('content')
    <div id="container-fluid">
        <div class="wrp">
            <div id="product" class="detail">
                <div class="pathway">
                    <ul>
                        <li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a itemprop="url" href="https://www.dangquangwatch.vn/" title="Home"><span itemprop="title">Trang chủ</span></a> </li>
                        <li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a itemprop="url" href="https://www.dangquangwatch.vn/sp/dong-ho-thuy-sy.html" title="Đồng hồ chính hãng"><span itemprop="title">Giỏ hàng</span></a></li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="left col-sm-8">
                    <div class="list">
                        <div class="title"> THÔNG TIN GIỎ HÀNG</div>
                        <div class="list_content">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th></th>
                                    <th>Sản phẩm</th>
                                    <th>Giá</th>
                                    <th>Số lượng</th>
                                    <th>Thành tiền</th>
                                </tr>
                                </thead>
                                <tbody style="font-size: 15px;">
                                @foreach($shopping as $key => $item)
                                    <tr>
                                        <td class="align-middle">
                                            <a href="{{ route('get.product.detail', Str::slug($item->name).'-'.$item->id) }}" class="">
                                                <img src="{{ pare_url_file($item->options->image) }}" style="width: 100px;height: 100px;" class="lazyload">
                                            </a>
                                        </td>
                                        <td class="align-middle">
                                            <a href="#"><strong>{{ $item->name }}</strong></a>
                                        </td>
                                        <td class="align-middle">
                                            <p class="text-success"><b>{{ number_format($item->price, 0, ',','.') }} đ</b></p>
                                            <p style="margin-top: 10px; ">
                                                @if($item->options->price_old)
                                                    <span style="text-decoration: line-through;" class="sale text-danger" style="margin-top: 5px; font-size: 15px;">{{ number_format($item->options->price_old, 0, ',','.') }} đ</span>
                                                    <span class="sale text-danger" style="margin-top: 5px; font-size: 15px;" >-{{ $item->options->sale }}%</span>
                                                @endif

                                            </p>
                                        </td>
                                        <td class="align-middle">
                                            <input style="width: 90px; text-align: center; margin-bottom: 10px;" type="number" min="1" class="form-control" id="qty" name="quatity" value="{{ $item->qty }}">
                                            <a href="{{ route('get.shopping.update', $key) }}" class="js-update-item-cart text-info" data-id-product="{{ $item->id }}">Cập nhật</a>
                                            <a href="{{ route('get.shopping.delete', $key) }}" style="margin-left: 10px; color: red;">Xoá</a>
                                        </td>
                                        <td class="text-success align-middle"><b>{{ number_format($item->qty*$item->price, 0, ',','.') }} đ</b></td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <p style="float: right;">Tổng tiền: <b>{{ \Cart::subtotal(0) }}</b></p>
                        </div>
                    </div>
                </div>
                <div class="right col-sm-4">
                    <div class="customer">
                        <div class="title">THÔNG TIN ĐẶT HÀNG</div>
                        <div class="customer_content">
                            <form class="form-group" action="{{ route('get.shopping.pay') }}" method="post">
                                @csrf
                                <div class="form-group">
                                    <label for="tst_name">Họ và tên <span class="text-danger">(*)</span></label>
                                    <input id="tst_name" name="tst_name" value="{{ get_data_user('web', 'name') }}" type="text" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="tst_phone">Điện thoại <span class="text-danger">(*)</span></label>
                                    <input name="tst_phone" id="tst_phone"  value="{{ get_data_user('web', 'phone') }}" type="text" class="form-control">
                                </div><div class="form-group">
                                    <label for="tst_address">Địa chỉ <span class="text-danger">(*)</span></label>
                                    <input name="tst_address" id="tst_address"  value="{{ get_data_user('web', 'address') }}" type="text" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="tst_email">Email</label>
                                    <input id="tst_email" name="tst_email"  value="{{ get_data_user('web', 'email') }}" type="text" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="note">Ghi chú thêm</label>
                                    <textarea name="tst_note" id="tst_note" cols="3" style="min-height:100px;" class="form-control"></textarea>
                                </div>
                                <div style="">
                                    <button style="font-size: 14px;" class="btn btn-primary left" name="tst_type" value="1" type="submit">Thanh toán khi nhận hàng</button>
                                    <button style="font-size: 14px;" class="btn btn-primary right"  name="tst_type" value="2" type="submit">Thanh toán online</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('script')
    <script type="text/javascript">
        $(function () {
            $(".js-update-item-cart").click(function (event                ) {
                event.preventDefault();
                let  $this = $(this);
                let url = $this.attr('href');
                let qty = $this.prev().val();
                let idProduct = $this.attr('data-id-product');
                console.log(url);

                if(url){
                    $.ajax({
                        url:url,
                        data:{
                            qty: qty,
                            idProduct: idProduct
                        }
                    }).done(function (result) {
                        alert(result.message)
                        window.location.reload();
                    })
                }
            })
        })
    </script>
@stop
