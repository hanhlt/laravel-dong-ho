@extends('layouts.app_master_frontend')
@section('content')
<link rel="stylesheet" type="text/css" href="https://www.dangquangwatch.vn/core/Smarty/templates/paging/style.css">
<div id="container">
    <div class="wrp">
        <div class="pathway" style="padding: 10px 0;">
            <ul>
                <li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a itemprop="url" href="{{ route('get.home') }}" title="Home"><span itemprop="title">Trang chủ</span></a></li>
                <li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a itemprop="url" href="" title="Bài viết"><span itemprop="title">Bài viết</span></a></li>
            </ul>
        </div>
        <div id="newslist">
            <div class="flex">
                <div class="left">
                    <div class="group">
                        @foreach( $articles as $article)
                            <div class="item">
                                <div class="wImage">
                                    <a href="{{ route('get.blog.detail',$article->a_slug.'-'.$article->id) }}" title="" class="image cover">
                                        <img data-src="https://www.dangquangwatch.vn/upload/article/949651051_top-3-dong-ho-co-2020.jpg" class="lazyload" alt="" src="https://www.dangquangwatch.vn/upload/article/949651051_top-3-dong-ho-co-2020.jpg"/>
                                    </a>
                                </div>
                                <div class="info">
                                    <a href="{{ route('get.blog.detail',$article->a_slug.'-'.$article->id) }}" title="{{ $article->a_name }}" class="name">{{ $article->a_name }}</a>
                                    <div class="cont dotdotdot">
                                        {{ $article->a_description }}
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    {{ $articles->links() }}
                </div>
                <div class="right">
                    @include('frontend.pages.blog.include.inc_siderbar')
                </div>
            </div>
            <div class="cb"></div>
        </div>
    </div>
</div>
@stop
