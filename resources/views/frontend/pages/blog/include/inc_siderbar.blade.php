<div class="productRight">
    <a href="#" title="" class="title">Sản phẩm bán chạy nhất</a>
    <div class="groupPro">
        @foreach($products as $product)
            <div class="item">
                <div class="wImage">
                    <a href="{{ route('get.product.detail',$product->pro_slug.'-'.$product->id) }}" title="" class="image">
                        <img data-src="{{ pare_url_file($product->pro_avatar) }}" class="lazyload" alt="Đồng hồ Aries Gold" src="{{ pare_url_file($product->pro_avatar) }}"/>
                    </a>
                </div>
                <div class="info">
                    <a href="{{ route('get.product.detail',$product->pro_slug.'-'.$product->id) }}" title="{{ $product->pro_name }}" class="cate">{{ $product->category->c_name ?? "[N/A]" }}</a>
                    <a href="{{ route('get.product.detail',$product->pro_slug.'-'.$product->id) }}" title="SaleOff" class="cate" style="color: red;font-weight: bold;">-{{ $product->pro_sale }}%</a>
                    <a href="{{ route('get.product.detail',$product->pro_slug.'-'.$product->id) }}" title="{{ $product->pro_name }}" class="namePro">{{ $product->pro_name }}</a>
                    @if ($product->pro_sale)
                        <p>
                            <span>Giá gốc: </span>
                            <span class="price" style="text-decoration: line-through;">{{ number_format($product->pro_price, 0, ',', '.') }}đ</span>
                        </p>
                        <span>Giá bán:  </span>
                        @php
                            $price_sale = ((100 - $product->pro_sale) * $product->pro_price) /100;
                        @endphp
                        <span class="new">{{ number_format($price_sale, 0, ',', '.') }} đ</span>
                    @else
                        <p>
                            <span>Giá gốc: </span>
                            <span class="price" style="text-decoration: line-through;">{{ number_format($product->price, 0, ',', '.') }}đ</span>
                        </p>
                    @endif

                </div>
            </div>
        @endforeach
    </div>
</div>
