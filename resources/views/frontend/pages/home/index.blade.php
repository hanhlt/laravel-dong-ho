@extends('layouts.app_master_frontend')
@section('content')
    @include('frontend.components.slider')

    <div id="container">
        <div id="product" class="hp">
            <div class="wrp">
                <div class="proNewst tabHome">
                    <div class="top">
                        <a href="#" title="" class="commonTitle">SẢN PHẨM MỚI</a>
                    </div>
                    <div class="bot active" id="proNewst1">
                        <div class="left">
                            <div class="wImage">
                                <a href="https://www.dangquangwatch.vn/sp/11620/Dong-ho-Diamond-D-DM61195IG-R.html" title="" class="image cover" target="_blank">
                                    <img data-src="https://www.dangquangwatch.vn/upload/homeads/1156094504_dong-ho-nu-thoi-trang.jpg" class="lazyload" alt="" src="/view/Css/icon/thumnail.jpg"/>
                                </a>
                            </div>
                        </div>
                        <div class="right">
                            @if(isset($productsNew))
                                @foreach($productsNew as $new)
                                    @include('frontend.components.product_item',['product'=> $new])
                                @endforeach
                            @endif
                        </div>
                    </div>
                </div>
                <div class="cb"></div>
                <div id="flash_sale">
                    <a href="https://www.facebook.com/TrungPhuNA" title="" class="image" target="_blank">
                        <img alt="" style="height:250px;" src="http://doan.phupt.net//uploads/2020/03/08/2020-03-08__banner.png" class="lazyload" width="100%">
                    </a>
                </div>
                <div id="pro_new2">
                    <div class="top">
                        <a href="/sp/Dong-ho-Epos-Swiss.html" title="Đồng hồ Epos" class="title">
                            <h3>Đồng hồ Epos Swiss
                                </h2>
                        </a>
                    </div>
                    <div class="bot">
                        <div class="left">
                            <div class="wImage">
                                <a href="https://www.dangquangwatch.vn/sp/14110/Dong-ho-Epos-Swiss-E-8000.700.20.85.15.html" title="Đồng hồ Epos" class="image cover">
                                    <img data-src="/upload/homeads/1003061035_dong-ho-nu-thuy-sy2.jpg" data-srcset="/upload/homeads/885219589_dong-ho-nu-thuy-sy1.jpg 480w, /upload/homeads/885219589_dong-ho-nu-thuy-sy1.jpg 640w, /upload/homeads/1003061035_dong-ho-nu-thuy-sy2.jpg 1024w" class="lazyload" alt="Đồng hồ Epos" src="/view/Css/icon/thumnail.jpg"/>
                                </a>
                            </div>
                        </div>
                        <div class="right">
                            <div class="itemPro">
                                <div class="wImage">
                                    <a href="https://www.dangquangwatch.vn/sp/11071/Dong-ho-Epos-Swiss-E-8000.700.20.85.30.html" title="ĐỒNG HỒ EPOS SWISS E-8000.700.20.85.30" class="image">
                                        <img data-src="/upload/product_hot/1383613745_dong-ho-nu-thuy-sy3.jpg" alt="ĐỒNG HỒ EPOS SWISS E-8000.700.20.85.30" src="/view/Css/icon/thumnail.jpg" class="lazyload"/>
                                    </a>
                                </div>
                                <a href="https://www.dangquangwatch.vn/sp/11071/Dong-ho-Epos-Swiss-E-8000.700.20.85.30.html" title="" class="name">ĐỒNG HỒ EPOS SWISS E-8000.700.20.85.30</a>
                                <p class="percent">-10%</p>
                                <p class="price">17.820.000 đ</p>
                                <p class="priceSale">19.800.000 đ</p>
                            </div>
                            <div class="itemPro">
                                <div class="wImage">
                                    <a href="https://www.dangquangwatch.vn/sp/11072/Dong-ho-Epos-Swiss-E-8000.700.22.88.32.html" title="ĐỒNG HỒ EPOS SWISS E-8000.700.22.88.32" class="image">
                                        <img data-src="/upload/product_hot/95046097_dong-ho-nu-thuy-sy77.jpg" alt="ĐỒNG HỒ EPOS SWISS E-8000.700.22.88.32" src="/view/Css/icon/thumnail.jpg" class="lazyload"/>
                                    </a>
                                </div>
                                <a href="https://www.dangquangwatch.vn/sp/11072/Dong-ho-Epos-Swiss-E-8000.700.22.88.32.html" title="" class="name">ĐỒNG HỒ EPOS SWISS E-8000.700.22.88.32</a>
                                <p class="percent">-10%</p>
                                <p class="price">20.520.000 đ</p>
                                <p class="priceSale">22.800.000 đ</p>
                            </div>
                            <div class="itemPro">
                                <div class="wImage">
                                    <a href="/sp/14529/Dong-ho-Epos-Swiss-E-3420.152.22.16.15.html" title="Đồng hồ Epos Swiss E-3420.152.22.16.15" class="image">
                                        <img data-src="/upload/product_hot/1028923289_dong-ho-thuy-sy14.jpg" alt="Đồng hồ Epos Swiss E-3420.152.22.16.15" src="/view/Css/icon/thumnail.jpg" class="lazyload"/>
                                    </a>
                                </div>
                                <a href="/sp/14529/Dong-ho-Epos-Swiss-E-3420.152.22.16.15.html" title="" class="name">Đồng hồ Epos Swiss E-3420.152.22.16.15</a>
                                <p class="percent">-10%</p>
                                <p class="price">28.890.000 đ</p>
                                <p class="priceSale">32.100.000 đ</p>
                            </div>
                            <div class="itemPro">
                                <div class="wImage">
                                    <a href="https://www.dangquangwatch.vn/sp/11552/dong-ho-Epos-Swiss-E-8000.700.22.65.15.html" title="ĐỒNG HỒ EPOS SWISS E-8000.700.22.65.15" class="image">
                                        <img data-src="/upload/product_hot/486239429_dong-ho-nu-thuy-sy76.jpg" alt="ĐỒNG HỒ EPOS SWISS E-8000.700.22.65.15" src="/view/Css/icon/thumnail.jpg" class="lazyload"/>
                                    </a>
                                </div>
                                <a href="https://www.dangquangwatch.vn/sp/11552/dong-ho-Epos-Swiss-E-8000.700.22.65.15.html" title="" class="name">ĐỒNG HỒ EPOS SWISS E-8000.700.22.65.15</a>
                                <p class="percent">-10%</p>
                                <p class="price">13.590.000 đ</p>
                                <p class="priceSale">15.100.000 đ</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="cb"></div>
                <div class="proSale">
                    <h2 class="title">
						<span>
							<a href="/sp/Dong-ho-Aries-Gold.html" title="Đồng hồ Aries Gold"> Đồng hồ Aries Gold</a>
						</span>
                    </h2>
                    <div class="group">
                        <div class="itemPro">
                            <div class="wImage">
                                <a href="/sp/17878/Dong-ho-Aries-Gold-AG-G1031-G-WG-+-AG-L1032-G-WG.html" title="Đồng hồ Aries Gold AG-G1031 G-WG + AG-L1032 G-WG" class="image">
                                    <img  class="lazyload" data-src="https://www.dangquangwatch.vn//upload/product_small/1726910040_dong-ho-chinh-hang-44.jpg" alt="Đồng hồ Aries Gold AG-G1031 G-WG + AG-L1032 G-WG" src="/view/Css/icon/thumnail.jpg"/>
                                </a>
                            </div>
                            <a href="/sp/17878/Dong-ho-Aries-Gold-AG-G1031-G-WG-+-AG-L1032-G-WG.html" title="Đồng hồ Aries Gold AG-G1031 G-WG + AG-L1032 G-WG" class="name">Đồng hồ Aries Gold AG-G1031 G-WG + AG-L1032 G-WG</a>
                            <p class="percent">-10%</p>
                            <p class="price">10.080.000 đ</p>
                            <p class="priceSale">11.200.000 đ</p>
                        </div>
                        <div class="itemPro">
                            <div class="wImage">
                                <a href="/sp/17873/Dong-ho-Aries-Gold-AG-L5038-G-W.html" title="Đồng hồ Aries Gold AG-L5038 G-W" class="image">
                                    <img  class="lazyload" data-src="https://www.dangquangwatch.vn//upload/product_small/313779583_ĐỒNG-HỒ-CHĨNH-HÃNG-3.jpg" alt="Đồng hồ Aries Gold AG-L5038 G-W" src="/view/Css/icon/thumnail.jpg"/>
                                </a>
                            </div>
                            <a href="/sp/17873/Dong-ho-Aries-Gold-AG-L5038-G-W.html" title="Đồng hồ Aries Gold AG-L5038 G-W" class="name">Đồng hồ Aries Gold AG-L5038 G-W</a>
                            <p class="percent">-10%</p>
                            <p class="price">2.902.500 đ</p>
                            <p class="priceSale">3.225.000 đ</p>
                        </div>
                        <div class="itemPro">
                            <div class="wImage">
                                <a href="/sp/17805/Dong-ho-Aries-Gold-AG-L5039Z-2TRG-W.html" title="Đồng hồ Aries Gold AG-L5039Z 2TRG-W" class="image">
                                    <img  class="lazyload" data-src="https://www.dangquangwatch.vn//upload/product_small/2063952782_dong-ho-chinh-hang-71.jpg" alt="Đồng hồ Aries Gold AG-L5039Z 2TRG-W" src="/view/Css/icon/thumnail.jpg"/>
                                </a>
                            </div>
                            <a href="/sp/17805/Dong-ho-Aries-Gold-AG-L5039Z-2TRG-W.html" title="Đồng hồ Aries Gold AG-L5039Z 2TRG-W" class="name">Đồng hồ Aries Gold AG-L5039Z 2TRG-W</a>
                            <p class="percent">-10%</p>
                            <p class="price">4.702.500 đ</p>
                            <p class="priceSale">5.225.000 đ</p>
                        </div>
                        <div class="itemPro">
                            <div class="wImage">
                                <a href="/sp/15979/Dong-ho-Aries-Gold-AG-G1001-S-BR-+-AG-L1002-S-BR.html" title="Đồng hồ Aries Gold AG-G1001 S-BR + AG-L1002 S-BR" class="image">
                                    <img  class="lazyload" data-src="https://www.dangquangwatch.vn//upload/product_small/877461008_đồng hồ đôi.jpg" alt="Đồng hồ Aries Gold AG-G1001 S-BR + AG-L1002 S-BR" src="/view/Css/icon/thumnail.jpg"/>
                                </a>
                            </div>
                            <a href="/sp/15979/Dong-ho-Aries-Gold-AG-G1001-S-BR-+-AG-L1002-S-BR.html" title="Đồng hồ Aries Gold AG-G1001 S-BR + AG-L1002 S-BR" class="name">Đồng hồ Aries Gold AG-G1001 S-BR + AG-L1002 S-BR</a>
                            <p class="percentBig">-20%</p>
                            <p class="price">5.520.000 đ</p>
                            <p class="priceSale">6.900.000 đ</p>
                        </div>
                        <div class="itemPro">
                            <div class="wImage">
                                <a href="/sp/15001/Dong-ho-Aries-Gold-AG-L5033Z-RG-BK.html" title="Đồng hồ Aries Gold AG-L5033Z RG-BK" class="image">
                                    <img  class="lazyload" data-src="https://www.dangquangwatch.vn//upload/product_small/582730192_đồng-hồ-nữ-thời-trang5.jpg" alt="Đồng hồ Aries Gold AG-L5033Z RG-BK" src="/view/Css/icon/thumnail.jpg"/>
                                </a>
                            </div>
                            <a href="/sp/15001/Dong-ho-Aries-Gold-AG-L5033Z-RG-BK.html" title="Đồng hồ Aries Gold AG-L5033Z RG-BK" class="name">Đồng hồ Aries Gold AG-L5033Z RG-BK</a>
                            <p class="percent">-10%</p>
                            <p class="price">3.577.500 đ</p>
                            <p class="priceSale">3.975.000 đ</p>
                        </div>
                        <div class="itemPro">
                            <div class="wImage">
                                <a href="/sp/14690/Dong-ho-Aries-Gold-AG-L5031-G-W-L.html" title="Đồng hồ Aries Gold AG-L5031 G-W-L" class="image">
                                    <img  class="lazyload" data-src="https://www.dangquangwatch.vn//upload/product_small/702352562_Untitled-1.jpg" alt="Đồng hồ Aries Gold AG-L5031 G-W-L" src="/view/Css/icon/thumnail.jpg"/>
                                </a>
                            </div>
                            <a href="/sp/14690/Dong-ho-Aries-Gold-AG-L5031-G-W-L.html" title="Đồng hồ Aries Gold AG-L5031 G-W-L" class="name">Đồng hồ Aries Gold AG-L5031 G-W-L</a>
                            <p class="percent">-10%</p>
                            <p class="price">3.802.500 đ</p>
                            <p class="priceSale">4.225.000 đ</p>
                        </div>
                        <div class="itemPro">
                            <div class="wImage">
                                <a href="/sp/14464/Dong-ho-Aries-Gold-AG-L5032Z-G-W-L.html" title="Đồng hồ Aries Gold AG-L5032Z G-W-L" class="image">
                                    <img  class="lazyload" data-src="https://www.dangquangwatch.vn//upload/product_small/602917256_dong-ho-chinh-hang55.jpg" alt="Đồng hồ Aries Gold AG-L5032Z G-W-L" src="/view/Css/icon/thumnail.jpg"/>
                                </a>
                            </div>
                            <a href="/sp/14464/Dong-ho-Aries-Gold-AG-L5032Z-G-W-L.html" title="Đồng hồ Aries Gold AG-L5032Z G-W-L" class="name">Đồng hồ Aries Gold AG-L5032Z G-W-L</a>
                            <p class="percent">-10%</p>
                            <p class="price">4.252.500 đ</p>
                            <p class="priceSale">4.725.000 đ</p>
                        </div>
                        <div class="itemPro">
                            <div class="wImage">
                                <a href="/sp/13451/Dong-ho-Aries-Gold-AG-G1013Z-2TG-S.html" title="Đồng hồ Aries Gold AG-G1013Z 2TG-S" class="image">
                                    <img  class="lazyload" data-src="https://www.dangquangwatch.vn//upload/product_small/1773979405_đòng-hồ-chính-hãng-6.jpg" alt="Đồng hồ Aries Gold AG-G1013Z 2TG-S" src="/view/Css/icon/thumnail.jpg"/>
                                </a>
                            </div>
                            <a href="/sp/13451/Dong-ho-Aries-Gold-AG-G1013Z-2TG-S.html" title="Đồng hồ Aries Gold AG-G1013Z 2TG-S" class="name">Đồng hồ Aries Gold AG-G1013Z 2TG-S</a>
                            <p class="percent">-10%</p>
                            <p class="price">2.677.500 đ</p>
                            <p class="priceSale">2.975.000 đ</p>
                        </div>
                    </div>
                </div>
                <div class="cb"></div>
            </div>
        </div>
    </div>
@stop
