@extends('layouts.app_master_frontend')
@section('content')
    <link rel="stylesheet" type="text/css" href="https://www.dangquangwatch.vn/core/Smarty/templates/paging/style.css">
    @if (isset($article))
    <div id="container">
        <div class="wrp">
            <div class="pathway" style="padding: 10px 0;">
                <ul>
                    <li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a itemprop="url" href="{{ route('get.home') }}" title="Home"><span itemprop="title">Trang chủ</span></a></li>
                    <li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a itemprop="url" href="" title="Bài viết"><span itemprop="title">Bài viết</span></a></li>
                    <li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a itemprop="url" href="" title="{{ $article->a_name }}"><span itemprop="title">{{ $article->a_name }}</span></a></li>
                </ul>
            </div>
            <div id="newslist">
                <div class="flex">
                        <div class="left">
                            <h1 class="titleNews">{{ $article->a_name }}</h1>
                            <h2 class="sapo">{{ $article->a_description }}</h2>
                            <div class="noidung">{{ $article->a_content }}</div>
                        </div>
                    <div class="right">
                        @include('frontend.pages.blog.include.inc_siderbar')
                    </div>
                </div>
                <div class="cb"></div>
            </div>
        </div>
    </div>
    @endif
@stop
