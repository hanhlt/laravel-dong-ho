@extends('layouts.app_master_frontend')
@section('content')
    <link rel="stylesheet" type="text/css" href="https://www.dangquangwatch.vn/core/Smarty/templates/paging/style.css">
    <div id="container">
        <div class="wrp">
            <div id="product" class="list">
                <div class="colleft">
                @include('frontend.pages.product.include.inc_sidebar')
                </div>
                <div class="colright">
                    <div class="pathway">
                        <ul>
                            <li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a itemprop="url" href="https://www.dangquangwatch.vn/" title="Home"><span itemprop="title">Trang chủ</span></a> </li>
                            <li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a itemprop="url" href="https://www.dangquangwatch.vn/sp/dong-ho-thuy-sy.html" title="Đồng hồ chính hãng"><span itemprop="title">Đồng hồ chính hãng</span></a></li>
                        </ul>
                    </div><div class="filterRight filter filter-tab">
                        <ul>
                            @for ($i = 1; $i <= 6; $i++)
                                <li class="{{ Request::get('price') == $i ? "active" : "" }}">
                                    <a href="{{ request()->fullUrlWithQuery(['price' => $i]) }}">
                                        {{ $i == 6 ? "Lớn hơn 10 triệu" : "Giá ". $i * 2 ." triệu" }}
                                    </a>
                                </li>
                            @endfor
                        </ul>
                    </div>
                    <div class="order">
                        <div class="price">
                            <span>Tổng số: {{ $products->total() }} sản phẩm</span>
                        </div>
                        <div class="right">
                            <div class="function odering">
                                <span class="openSubOrder">Sắp xếp <i class="fas fa-caret-down"></i></span>
                                <div class="sub filter">
                                    <div class="group">
                                        <ul>
                                            <li>
                                                <a href="/sp/dong-ho-thuy-sy.html?&s=2" title="" class="active">
                                                    <span>Giá thấp đến cao</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="/sp/dong-ho-thuy-sy.html?&s=1" title="" class="active">
                                                    <span>Giá cao xuống thấp</span>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                    <a href="javascript://" class="closeSub"><i class="far fa-times-circle"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="cb h5"></div>
                    <div class="group">
                        @foreach($products as $product)
                            @include('frontend.components.product_item', ['product' =>$product ])
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="cb"></div>
        </div>
    </div>
@stop
