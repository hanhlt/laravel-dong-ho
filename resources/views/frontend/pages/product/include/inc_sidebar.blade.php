<input type="hidden" name="strUrl99" id="strUrl99" value="/sp/dong-ho-thuy-sy.html">
<input type="hidden" name="iSort" id="iSort" value="">
<div class="titleCol">Thương hiệu</div>
<style type="text/css">
    .item_content .active a{
        color: red;
    }
    .filter-tab .active a{
        color: red;
    }
</style>
@if(isset($country))
    <div class="titleCol">Xuất xứ</div>
    <div class="filter">
        <ul class="item_content" id="LoaiGroup">
            @foreach ($country as $key => $item)
                <li class="{{ Request::get('country') == $key ? "active" : "" }}">
                    <a href="{{ request()->fullUrlWithQuery(['country' => $key]) }}">
                        <h2><span>{{ $item }}</span></h2>
                    </a>
                </li>
            @endforeach
        </ul>
    </div>
@endif

@if(isset($attributes))
    @foreach($attributes as $key => $attribute )
        <div class="titleCol">{{ $key }}</div>
        <div class="filter">
            <ul class="item_content" id="LoaiGroup">
                @foreach ($attribute as $item)
                    <li class="{{ Request::get('attr_'.$item['atb_type']) == $item['id'] ? "active" : "" }}">
                        <a href="{{ request()->fullUrlWithQuery(['attr_'.$item['atb_type'] => $item['id']]) }}">
                            <h2><span>{{ $item['atb_name'] }}</span></h2>
                        </a>
                    </li>
                @endforeach
            </ul>
        </div>
    @endforeach
@endif
