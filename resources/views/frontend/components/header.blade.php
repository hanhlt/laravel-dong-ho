    <section class="top-header desktop">
         <div class="container">
            <div class="content">
               <div class="left"> <a href="" title="Chăm sóc khách hàng" rel="nofollow">Chăm sóc khách hàng</a> <a href="" title="Kiểm tra đơn hàng" rel="nofollow">Kiểm tra đơn hàng</a> </div>
               <div class="right"> <a href="{{ route('get.register') }}">Đăng ký</a> <a href="{{ route('get.login') }}">Đăng nhập</a> </div>
            </div>
         </div>
      </section>
      <section class="top-header mobile">
         <div class="container">
            <div class="content">
               <div class="left"> <a href="" title="Chăm sóc khách hàng" rel="nofollow">Chăm sóc khách hàng</a> <a href="" title="Kiểm tra đơn hàng" rel="nofollow">Kiểm tra đơn hàng</a> <a href="{{ route('get.register') }}">Đăng ký</a> <a href="{{ route('get.login') }}">Đăng nhập</a> </div>
            </div>
         </div>
      </section>
      <div class="commonTop">
         <div id="headers">
            <div class="container header-wrapper">
               <div class="logo"> <a href="/" class="desktop"> <img src="{{ asset('images/logo.png') }}" style="height: 35px;" alt="Home"> </a> <span class="menu js-menu-cate"><i class="fa fa-list-ul"></i> </span> </div>
               <div class="search">
                  <form action="{{ route('get.product.list', ['k' => Request::get('k')]) }}" role="search" method="GET">
                      <input type="text" name="k" value="{{ Request::get('k') }}" class="form-control" placeholder="Tìm kiếm sản phẩm ...">
                      <button type="submit" class="btnSearch"> <i class="fa fa-search"></i> <span>Tìm kiếm</span> </button>
                  </form>
               </div>
               <ul class="right">
                  <li> <a href="{{ route('get.shopping.list') }}" title="Giỏ hàng"> <i class="fas fa-app-store"></i> <span class="text"> <span class="">Giỏ hàng ({{ \Cart::count() }})</span> <span></span> </span> </a> </li>
                  <li class="desktop"> <a href="tel:18006005" title=""> <i class="fas fa-accusoft"></i> <span class="text"> <span class="">Hotline</span> <span>1800.6005</span> </span> </a> </li>
               </ul>
               <div id="menu-main" class="container" style="display: none;">
                  <ul class="menu-list">
                  @if(isset($categories))
                        @foreach($categories as $category)
                            <li>
                                <a href="{{ route('get.category.list', $category->c_slug.'-'.$category->id) }}" title="{{$category->c_name}}" class="js-open-menu">
                                <img src="" alt="{{$category->c_name}}">
                                <span>{{$category->c_name}}</span> </a>
                            </li>
                        @endforeach
                    @endif
                </ul>
               </div>
            </div>
         </div>
      </div>
{{--        <div id="content-slide">--}}
{{--         <div class="spinner">--}}
{{--            <div class="rect1"></div>--}}
{{--            <div class="rect2"></div>--}}
{{--            <div class="rect3"></div>--}}
{{--            <div class="rect4"></div>--}}
{{--            <div class="rect5"></div>--}}
{{--         </div>--}}
{{--      </div>--}}
