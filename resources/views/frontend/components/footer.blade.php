<div id="footer">
    <div class="container footer">
        <div class="footer__left">
            <div class="top">
                <div class="item">
                    <div class="title">Về chúng tôi</div>
                    <ul>
                        <li>
                            <a href="http://doan.phupt.net/bai-viet">Bài viết</a>
                        </li>
                        <li>
                            <a href="http://doan.phupt.net/san-pham">Sản phẩm</a>
                        </li>
                        <li>
                            <a href="http://doan.phupt.net/account/register">Đăng ký</a>
                        </li>
                        <li>
                            <a href="http://doan.phupt.net/account/login">Đăng nhập</a>
                        </li>
                    </ul>
                </div>
                <div class="item">
                    <div class="title">Tin tức</div>
                    <ul>
                        <li>
                            <a title="Tin tức - Sự kiện"
                               href="http://doan.phupt.net/menu/tin-tuc-su-kien-1"> Tin tức - Sự kiện </a>
                        </li>
                        <li>
                            <a title="Tuyển dụng"
                               href="http://doan.phupt.net/menu/tuyen-dung-2"> Tuyển dụng </a>
                        </li>
                        <li>
                            <a href="http://doan.phupt.net/lien-he">Liên hệ</a>
                        </li>
                    </ul>
                </div>
                <div class="item">
                    <div class="title">Chính sách</div>
                    <ul>
                        <li>
                            <a href="http://doan.phupt.net/huong-dan-mua-hang">Hướng dẫn mua hàng</a>
                        </li>
                        <li>
                            <a href="http://doan.phupt.net/chinh-sach-doi-tra">Chính sách đổi trả</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="bot">
                <div class="social">
                    <div class="title">KẾT NỐI VỚI CHÚNG TÔI</div>
                    <p>
                        <a href="" class="fa fa fa-youtube"></a>
                        <a href="" class="fa fa-facebook-official"></a>
                        <a href="" class="fa fa-twitter"></a>
                    </p>
                </div>
            </div>
        </div>
        <div class="footer__mid">
            <div class="title">Hệ thống cửa hàng</div>
            <div class="image"></div>
        </div>
        <div class="footer__right">
            <div class="title">Fanpage của chúng tôi</div>
            <div class="image"></div>
        </div>
    </div>
</div>
<script src="https://kit.fontawesome.com/49f1bd26af.js" crossorigin="anonymous"></script>
<script src="https://www.dangquangwatch.vn/min/?g=js"></script>
