@extends('layouts.app_master_admin')
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-left">
                        <li class="breadcrumb-item"><a href="{{ route('admin.index') }}">Trang chủ</a></li>
                        <li class="breadcrumb-item active">Bài viết</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">
                    <a href="{{ route('admin.article.create') }}" class="btn btn-primary"><i class="fa fa-plus"></i> Thêm bài viết</a>
                </h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body p-0">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th >Hình ảnh</th>
                        <th>Tiêu đề</th>
                        <th>Menu</th>
                        <th>Hot</th>
                        <th>Trạng thái</th>
                        <th>Lượt xem</th>
                        <th>Hành động</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(isset($articles))
                        @foreach($articles as $article)
                        <tr>
                            <td>{{ $article->id }}</td>
                            <td><img src="{{ pare_url_file($article->a_avatar) }}" height="100px" width="100px"></td>
                            <td>{{ $article->a_name }}</td>
                            <td>
                                <span class="badge badge-success">{{ $article->menu->mn_name ?? "[N/A]" }}</span>
                            </td>
                            <td>
                                @if($article->a_hot == 1)
                                    <a href="{{ route('admin.article.hot', $article->id) }}" class="badge badge-info">Hot</a>
                                @else
                                    <a href="{{ route('admin.article.hot', $article->id) }}" class="badge badge-secondary">Không</a>
                                @endif
                            </td>
                            <td>
                                @if($article->a_active == 1)
                                    <a href="{{ route('admin.article.active', $article->id) }}" class="badge badge-info">Hiển thị</a>
                                @else
                                    <a href="{{ route('admin.article.active', $article->id) }}" class="badge badge-secondary">Không</a>
                                @endif
                            </td>
                            <td>{{ $article->a_view }}</td>
                            <td>
                                <a href="{{ route('admin.article.update', $article->id) }}" class="btn btn-sm btn-success"> <i class="fa fa-pen"></i> Cập nhật</a>
                                <a href="{{ route('admin.article.delete', $article->id) }}" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i> Xoá bỏ</a>
                            </td>
                        </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </section>
@stop
