@extends('layouts.app_master_admin')
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-left">
                        <li class="breadcrumb-item"><a href="{{ route('admin.index') }}">Trang chủ</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('admin.article.index') }}">Bài viết</a></li>
                        <li class="breadcrumb-item active">Cập nhật bài viết</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            @if(isset($article))
            <form role="form" action="" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="row">
                    <!-- left column -->
                    <div class="col-md-8">
                        <!-- general form elements -->
                        <div class="card card-dark">
                            <div class="card-header">
                                <h3 class="card-title">Thông tin cơ bản</h3>
                            </div>
                            <!-- /.card-header -->
                            <!-- form start -->
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="a_name">Tiêu đề (<span class="text-danger">*</span>)</label>
                                    <input type="text" class="form-control {{ $errors->first('a_name') ? 'is-invalid' : '' }}" name="a_name" value="{{ $article->a_name }}">
                                    @if($errors->first('a_name'))
                                        <span class="text-danger">{{ $errors->first('a_name') }}</span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="a_description">Mô tả</label>
                                    <textarea name="a_description" class="form-control {{ $errors->first('a_description') ? 'is-invalid' : '' }}">{{ $article->a_description }}</textarea>
                                    @if($errors->first('a_description'))
                                        <span class="text-danger">{{ $errors->first('a_description') }}</span>
                                    @endif
                                </div>

                                <div class="form-group">
                                    <label for="a_menu_id">Danh mục (<span class="text-danger">*</span>)</label>
                                    <select name="a_menu_id" class="form-control" >
                                        @foreach($menus as $menu)
                                            @foreach($menus as $menu)
                                                @if($article->a_menu_id == $menu->id)
                                                    <option value="{{ $menu->id }}" selected="selected">{{ $menu->mn_name }}</option>
                                                @else
                                                    <option value="{{ $menu->id }}">{{ $menu->mn_name }}</option>
                                                @endif
                                            @endforeach
                                            <option value="{{ $menu->id }}">{{ $menu->mn_name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->

                        <div class="card card-dark">
                            <div class="card-header">
                                <h3 class="card-title">Nội dung</h3>
                            </div>
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="content">Content</label>
                                    <textarea class="form-control  {{ $errors->first('a_content') ? 'is-invalid' : '' }}" name="a_content" id="editor">{{ $article->a_content }}</textarea>
                                    @if($errors->first('a_content'))
                                        <span class="text-danger">{{ $errors->first('a_content') }}</span>
                                    @endif
                                </div>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->

                    </div>
                    <!--/.col (left) -->
                    <!-- right column -->
                    <div class="col-md-4">
                        <!-- general form elements disabled -->
                        <div class="card card-dark">
                            <div class="card-header">
                                <h3 class="card-title">Ảnh sản phẩm</h3>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body block-images">
                                <img src="https://scontent-hkg3-2.xx.fbcdn.net/v/t1.0-9/82881993_2531032927153352_2689820468524875776_n.jpg?_nc_cat=111&_nc_ohc=fc3mkkPsA1UAX_EIzMe&_nc_ht=scontent-hkg3-2.xx&oh=00b494e941448c3a891c7f221c754331&oe=5EB5AFBB" style="height: 200px; width: 200px;" alt="" class="img-thumbnail">
                                <div class="form-group mt-3">
                                    <a style="position: relative" class="btn btn-dark" href="javascript:;">Chọn ảnh..
                                        <input style="position: absolute;z-index: 2;top: 0;left: 0;filter: alpha(opacity=0);-ms-filter: &quot;progid:DXImageTransform.Microsoft.Alpha(Opacity=0)&quot;;opacity: 0;background-color: transparent;color: transparent;" type="file" class="js-upload" name="a_avatar">
                                    </a> <span class="badge badge-info" id="upload-file-info"></span>
                                </div>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                    <!--/.col (right) -->
                </div>
                <div class="col-ms-12 card p-3">
                    <div class="small-box-footer text-center">
                        <a href="{{ route('admin.article.index') }}" class="btn btn-danger"> <i class="fa fa-arrow-left"></i>Quay lại</a>
                        <button type="" class="btn btn-success"> <i class="fa fa-save"></i>  Cập nhật</button>
                    </div>
                </div>
            </form>
            @endif
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
    <script src="{{ asset('root/ckeditor5/ckeditor.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
        ClassicEditor
            .create( document.querySelector( '#editor' ) )
            .catch( error => {
                console.error( error );
            } );
    </script>
@stop
