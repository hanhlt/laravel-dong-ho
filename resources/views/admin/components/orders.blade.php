<table class="table">
    <thead>
    <tr>
        <th>#</th>
        <th>Sản phẩm</th>
        <th>Hình</th>
        <th>Giá</th>
        <th>Số lượng</th>
        <th>Tổng tiền</th>
        <th>Thao tác</th>
    </tr>
    </thead>
    <tbody>
    @foreach($orders as $item)
    <tr>
        <td>{{ $item->id }}</td>
        <td><a href="">{{ $item->product->pro_name?? "[N\A]" }}</a> </td>
        <td>
            <img src="{{ pare_url_file($item->product->pro_avatar ?? "") }}" style="width: 100px; height: 100px;">
        </td>
        <td>{{ number_format($item->od_price,0,',','.') }} đ</td>
        <td>{{ $item->od_qty }}</td>
        <td>{{ number_format($item->od_price * $item->od_qty,0,',','.') }} đ</td>
        <td>
            <a href="{{ route('ajax.admin.transaction.delete', $item->id) }}" class="btn btn-danger js-order-delete-item">Xoá bỏ</a>
        </td>
    </tr>
    @endforeach
    </tbody>
</table>
