@extends('layouts.app_master_admin')
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-left">
                        <li class="breadcrumb-item"><a href="{{ route('admin.index') }}">Trang chủ</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('admin.product.index') }}">Sản phẩm</a></li>
                        <li class="breadcrumb-item active">Thêm mới</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <form role="form" action="" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="row">
                    <!-- left column -->
                    <div class="col-md-12">
                        <!-- general form elements -->
                        <div class="card card-dark">
                            <div class="card-header">
                                <h3 class="card-title">Thông tin cơ bản</h3>
                            </div>
                            @if($slide)
                                <div class="card-body">
                                    <div class="form-group col-sm-8">
                                        <label for="sd_title">Title (<span class="text-danger">*</span>)</label>
                                        <input type="text" value="{{ $slide->sd_title }}" class="form-control {{ $errors->first('sd_title') ? 'is-invalid' : '' }}" name="sd_title" placeholder="Tiêu đề...">
                                        @if($errors->first('sd_title'))
                                            <span class="text-danger">{{ $errors->first('sd_title') }}</span>
                                        @endif
                                    </div>
                                    <div class="form-group col-sm-8">
                                        <label for="sd_link">Link (<span class="text-danger">*</span>)</label>
                                        <input type="text" value="{{ $slide->sd_link }}" class="form-control {{ $errors->first('sd_link') ? 'is-invalid' : '' }}" name="sd_link" placeholder="Link...">
                                        @if($errors->first('sd_link'))
                                            <span class="text-danger">{{ $errors->first('sd_link') }}</span>
                                        @endif
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="row">
                                            <div class="form-group col-sm-6">
                                                <label for="sd_target">Target (<span class="text-danger">*</span>)</label>
                                                <select class="form-control" name="sd_target">
                                                    <option value="1" {{ $slide->sd_target == 1 ? "selected='selected'" : "" }}>_black</option>
                                                    <option value="2" {{ $slide->sd_target == 2 ? "selected='selected'" : "" }}>_self</option>
                                                    <option value="3" {{ $slide->sd_target == 3 ? "selected='selected'" : "" }}>_parent</option>
                                                    <option value="4" {{ $slide->sd_target == 4 ? "selected='selected'" : "" }}>_top</option>
                                                </select>
                                            </div>
                                            <div class="form-group col-sm-6">
                                                <label for="sd_sort">Sort (<span class="text-danger">*</span>)</label>
                                                <input type="number" value="{{ $slide->sd_sort }}" class="form-control {{ $errors->first('sd_sort') ? 'is-invalid' : '' }}" name="sd_sort" placeholder="0">
                                                @if($errors->first('sd_link'))
                                                    <span class="text-danger">{{ $errors->first('sd_sort') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group col-sm-12 block-images">
                                            <div style="margin-bottom: 10px;">
                                                <img src="{{ asset('images/no-image.jpg') }}" or=null;this.src='{{ asset('images/no-image.jpg') }}';" class="img-thumbnail"
                                                     style="width: 100%; height: 600px">
                                            </div>
                                            <div class="form-group mt-3">
                                                <a style="position: relative" class="btn btn-dark" href="javascript:;">Chọn ảnh..
                                                    <input style="position: absolute;z-index: 2;top: 0;left: 0;filter: alpha(opacity=0);-ms-filter: &quot;progid:DXImageTransform.Microsoft.Alpha(Opacity=0)&quot;;opacity: 0;background-color: transparent;color: transparent;" type="file" class="js-upload" name="sd_image">
                                                </a> <span class="badge badge-info" id="upload-file-info"></span>
                                            </div>
                                        </div>
                                    </div>


                                </div>
                                <!-- /.card-body -->
                            @endif
                        </div>
                        <!-- /.card -->

                    </div>
                </div>
                <div class="col-ms-12 card p-3">
                    <div class="small-box-footer text-center">
                        <a href="{{ route('admin.slide.index') }}" class="btn btn-danger"> <i class="fa fa-arrow-left"></i>Quay lại</a>
                        <button type="submit" class="btn btn-success"> <i class="fa fa-save"></i> Cập nhật</button>
                    </div>
                </div>
            </form>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
@stop
