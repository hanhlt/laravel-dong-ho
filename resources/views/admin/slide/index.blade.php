@extends('layouts.app_master_admin')
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-left">
                        <li class="breadcrumb-item"><a href="{{ route('admin.index') }}">Trang chủ</a></li>
                        <li class="breadcrumb-item active">Slide</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">
                    <a href="{{ route('admin.slide.create') }}" class="btn btn-primary"><i class="fa fa-plus"></i> Thêm mới</a>
                </h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body p-0">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th >Name</th>
                        <th>Banner</th>
                        <th>Status</th>
                        <th>Sort</th>
                        <th>Time</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($slides as $slide)
                        <tr>
                            <td>{{ $slide->id }}</td>
                            <td>{{ $slide->sd_title }}</td>
                            <td>{{ $slide->id }}</td>
                            <td>
                                @if($slide->sd_active ==1)
                                    <a href="{{ route('admin.slide.active', $slide->id) }}"><span class="badge badge-info">Active</span></a>
                                @else
                                    <a href="{{ route('admin.slide.active', $slide->id) }}"><span class="badge badge-secondary">None</span></a>
                                @endif
                            </td>
                            <td>{{ $slide->sd_sort }}</td>
                            <td>{{ $slide->created_at }}</td>
                            <td>
                                <a href="{{ route('admin.slide.update', $slide->id) }}" class="btn btn-sm btn-success"> <i class="fa fa-pen"></i> Cập nhật</a>
                                <a href="{{ route('admin.slide.delete', $slide->id) }}" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i> Xoá bỏ</a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </section>
@stop
