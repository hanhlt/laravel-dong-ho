@extends('layouts.app_master_admin')
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-left">
                        <li class="breadcrumb-item"><a href="{{ route('admin.index') }}">Trang chủ</a></li>
                        <li class="breadcrumb-item active">Danh mục</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">
                    <a href="{{ route('admin.menu.create') }}" class="btn btn-primary"><i class="fa fa-plus"></i> Thêm mới</a>
                </h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body p-0">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Menu</th>
                        <th>Hình ảnh</th>
                        <th>Mô tả</th>
                        <th>Trạng thái</th>
                        <th>Hot</th>
                        <th>Hành động</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($menus as $menu)
                        <tr>
                            <td>{{ $menu->id }}</td>
                            <td>{{ $menu->mn_name }}</td>
                            <td></td>
                            <td>{{ $menu->mn_description }}</td>
                            <td>
                                @if($menu->mn_status == 1)
                                    <a href="{{ route('admin.menu.status', $menu->id) }}" class="badge badge-info">Hiện thị</a>
                                @else
                                    <a href="{{ route('admin.menu.status', $menu->id) }}" class="badge badge-secondary">Không</a>
                                @endif
                            </td>
                            <td>
                                @if($menu->mn_hot == 1)
                                    <a href="{{ route('admin.menu.hot', $menu->id) }}" class="badge badge-info">Hot</a>
                                @else
                                    <a href="{{ route('admin.menu.hot', $menu->id) }}" class="badge badge-secondary">Không</a>
                                @endif
                            </td>
                            <td>
                                <a href="{{ route('admin.menu.update', $menu->id) }}" class="btn btn-sm btn-success"> <i class="fa fa-pen"></i> Cập nhật</a>
                                <a href="{{ route('admin.menu.delete', $menu->id) }}" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i> Xoá bỏ</a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </section>
@stop
