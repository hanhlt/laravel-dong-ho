@extends('layouts.app_master_admin')
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-left">
                        <li class="breadcrumb-item"><a href="{{ route('admin.index') }}">Trang chủ</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('admin.menu.index') }}">Menu</a></li>
                        <li class="breadcrumb-item active">Tạo menu</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Tạo menu</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body p-0">
                    <div class="card-body">
                        <form action="" method="POST">
                            @csrf
                            <div class="form-group">
                                <label for="mn_name">Menu (<span class="text-danger">*</span>)</label>
                                <input type="text" class="form-control {{ $errors->first('mn_name') ? 'is-invalid' : '' }}" name="mn_name">
                                @if($errors->first('mn_name'))
                                    <span class="text-danger">{{ $errors->first('mn_name') }}</span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="mn_description">Mô tả </label>
                                <input type="text" class="form-control {{ $errors->first('mn_description') ? 'is-invalid' : '' }}" name="mn_description">
                                @if($errors->first('mn_description'))
                                    <span class="text-danger">{{ $errors->first('mn_description') }}</span>
                                @endif
                            </div>
                            <div class="col-ms-12">
                                <div class="small-box-footer">
                                    <a href="{{ route('admin.menu.index') }}" class="btn btn-danger"> <i class="fa fa-arrow-left"></i>Quay lại</a>
                                    <button type="submit" class="btn btn-success"> <i class="fa fa-save"></i>Thêm</button>
                                </div>
                            </div>
                        </form>
                    </div>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </section>
@stop
