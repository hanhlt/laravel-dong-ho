@extends('layouts.app_master_admin')
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-left">
                        <li class="breadcrumb-item"><a href="{{ route('admin.index') }}">Trang chủ</a></li>
                        <li class="breadcrumb-item active">Đơn hàng</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="card">
            <div class="card-title">
                <form class="form-inline" style="padding: 10px;">
                    <input type="text" class="form-control" value="{{ Request::get('id') }}" name="id" placeholder="ID">
                    <input type="text" class="form-control ml-2" value="{{ Request::get('email') }}" name="email" placeholder="Email">
                    <select name="type" class="form-control ml-2">
                        <option>Phân loại khách</option>
                        <option value="1" {{ Request::get('type') == 1 ?  "selected='selected'" : '' }}>Thành viên</option>
                        <option value="2" {{ Request::get('type') == 2 ?  "selected='selected'" : '' }}>Khách</option>
                    </select>
                    <select name="status" class="form-control ml-2">
                        <option>Trạng thái</option>
                        <option value="1" {{ Request::get('status') == 1 ?  "selected='selected'" : '' }}>Tiếp nhận</option>
                        <option value="2" {{ Request::get('status') == 2 ?  "selected='selected'" : '' }}>Đang vận chuyển</option>
                        <option value="3" {{ Request::get('status') == 3 ?  "selected='selected'" : '' }}>Đã bàn giao</option>
                        <option value="-1" {{ Request::get('status') == -1 ?  "selected='selected'" : '' }}>Huỷ bỏ</option>
                    </select>
                    <button type="submit" class="btn btn-success ml-2"><i class="fa fa-search"></i> Tìm kiếm</button>
                    <button type="submit" name="export" value="true" class="btn btn-success ml-2">
                        <i class="fa fa-save"></i> Export
                    </button>
                </form>
            </div>
            <!-- /.card-header -->
            <div class="card-body p-0">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Thông tin</th>
                        <th>Tổng tiền</th>
                        <th>Tài khoản</th>
                        <th>Trạng thái</th>
                        <th>Ngày tạo</th>
                        <th>Thao tác</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(isset($transactions))
                        @foreach($transactions as $transaction)
                            <tr>
                                <td>{{ $transaction->id }}</td>
                                <td>
                                    <ul>
                                        <li>Họ tên: {{ $transaction->tst_name }}</li>
                                        <li>Email: {{ $transaction->tst_email }}</li>
                                        <li>Điện thoại: {{ $transaction->tst_phone }}</li>
                                        <li>Địa chỉ: {{ $transaction->tst_address }}</li>
                                    </ul>
                                </td>
                                <td>{{ number_format($transaction->tst_total_money, 0, ',','.') }} đ</td>
                                <td>
                                    @if($transaction->tst_user_id)
                                        <span class="badge btn-success">Thành viên</span>
                                    @else
                                        <span class="badge btn-primary">Khách</span>
                                    @endif
                                </td>
                                <td>
                                    <span class="badge badge-{{ $transaction->getStatus($transaction->tst_status)['class'] }}">
                                        {{ $transaction->getStatus($transaction->tst_status)['name'] }}
                                    </span>
                                </td>
                                <td>{{ $transaction->created_at }}</td>
                                <td>
                                    <a data-id="{{ $transaction->id }}" href="{{ route('ajax.admin.transaction.detail', $transaction->id) }}" class="btn btn-sm btn-info js-preview-transaction"><i class="fa fa-eye"></i> Xem đơn hàng</a>
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-sm btn-success">Thao tác</button>
                                        <button type="button" class="btn btn-success btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                            <span class="caret"></span>
                                            <span class="sr-only">Toggle Dropdown</span>
                                        </button>
                                        <ul class="dropdown-menu" role="menu">
                                            <li>
                                                <a href="{{ route('admin.transaction.delete', $transaction->id) }}"  style="padding: 10px;font-size: 15px;" class="text-danger"><i class="fa fa-trash"></i> Xoá bỏ</a>
                                            </li>
                                            <li class="dropdown-divider"></li>
                                            <li>
                                                <a href="{{ route('admin.action.transaction',['process', $transaction->id]) }}" style="padding: 10px;font-size: 15px;" class="text-info"><i class="fa fa-ban"></i> Đang vẫn chuyển</a>
                                            </li>
                                            <li class="dropdown-divider"></li>
                                            <li>
                                                <a href="{{ route('admin.action.transaction',['success', $transaction->id]) }}" style="padding: 10px;font-size: 15px;" class="text-success"><i class="fa fa-ban"></i> Đã chuyển</a>
                                            </li>
                                            <li class="dropdown-divider"></li>
                                            <li>
                                                <a href="{{ route('admin.action.transaction',['cancel', $transaction->id]) }}" style="padding: 10px;font-size: 15px;" class="text-warning"><i class="fa fa-ban"></i> Huỷ</a>
                                            </li>
                                        </ul>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
            <!-- /.card-body -->
            <div class="small-box-footer">

            </div>
        </div>
        <!-- /.card -->
    </section>

    <div class="modal fade fade" id="modal-preview-transaction">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Chi tiết đơn hàng <b id="idTransaction">#1</b></h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="content"></div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                    <button type="button" class="btn btn-primary">Lưu</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
@stop
