@extends('layouts.app_master_frontend')
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
@section('content')
    <div id="container">
        <div class="wrp">
            <div>
                <div class="pathway">
                    <ul>
                        <li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a itemprop="url" href="{{ route('get.home') }}" title=""><span itemprop="title">Trang chủ</span></a> </li>
                        <li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a itemprop="url" href="" title=""><span itemprop="title">Đăng nhập</span></a></li>
                    </ul>
                </div>
                <div class="auth" style="background: white;">
                    <div class="row">
                        <div class="col-sm-8">
                            <form class="form-group" action="" method="post" style="padding: 30px;">
                                @csrf
                                <div class="form-group">
                                    <label for="name">Email <span class="text-danger">(*)</span></label>
                                    <input type="text" id="email" name="email" class="form-control" placeholder="lathanhhanh@gmail.com">
                                    @if($errors->first('email'))
                                        <span class="text-danger">{{ $errors->first('email') }}</span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="name">Mật khẩu <span class="text-danger">(*)</span></label>
                                    <input type="password" id="password" name="password" class="form-control" placeholder="*******">
                                    @if($errors->first('password'))
                                        <span class="text-danger">{{ $errors->first('password') }}</span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <input type="checkbox" id="name" name="name" class="form-group"> Ghi nhớ
                                </div>
                                <div>
                                    <button class="btn btn-success">Đăng nhập</button>
                                </div>
                                <div class="form-group mt-3">
                                    <a href="{{ route('get.register')  }}" class="text-success">Quên mật khẩu</a>
                                </div>
                            </form>
                        </div>
                        <div class="col-sm-4 p-5">
                            Chưa có tài khoản <a href="{{ route('get.register')  }}" class="text-success">Đăng ký</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
