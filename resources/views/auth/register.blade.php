@extends('layouts.app_master_frontend')
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
@section('content')
    <div id="container">
        <div class="wrp">
            <div class="pathway">
                <ul>
                    <li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a itemprop="url" href="{{ route('get.home') }}" title=""><span itemprop="title">Trang chủ</span></a> </li>
                    <li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a itemprop="url" href="" title=""><span itemprop="title">Đăng ký</span></a></li>
                </ul>
            </div>
            <div class="auth" style="background: white;">
               <div class="row">
                  <div class="col-sm-8">
                      <div style="padding: 30px;">
                          <form class="form-group" action="" method="post">
                              @csrf
                              <div class="form-group">
                                  <label for="name">Họ và tên <span class="text-danger">(*)</span></label>
                                  <input type="text" id="name" name="name" value="{{ old('name') }}" class="form-control mb-2" placeholder="Sky Gapo Lotus">
                                  @if($errors->first('name'))
                                      <span class="text-danger">{{ $errors->first('name') }}</span>
                                  @endif
                              </div>
                              <div class="form-group">
                                  <label for="name">Email <span class="text-danger">(*)</span></label>
                                  <input type="email" id="email" value="{{ old('email') }}" name="email" class="form-control mb-2" placeholder="vidu@lotus.vn">
                                  @if($errors->first('email'))
                                      <span class="text-danger mt-2">{{ $errors->first('email') }}</span>
                                  @endif
                              </div>
                              <div class="form-group">
                                  <label for="password">Mật khẩu<span class="text-danger">(*)</span></label>
                                  <input type="password" id="password" name="password" class="form-control mb-2" placeholder="********">
                                  @if($errors->first('password'))
                                      <span class="text-danger">{{ $errors->first('password') }}</span>
                                  @endif
                              </div>
                              <div class="form-group">
                                  <label for="name">Số điện thoại <span class="text-danger">(*)</span></label>
                                  <input type="text" id="phone" name="phone"  value="{{ old('phone') }}" class="form-control mb-2" placeholder="0356728828">
                                  @if($errors->first('phone'))
                                      <span class="text-danger">{{ $errors->first('phone') }}</span>
                                  @endif
                              </div>
                              <div>
                                  <button class="btn btn-success">Đăng ký</button>
                              </div>
                          </form>
                      </div>
                  </div>
                   <div class="col-sm-4 p-5">
                       Đã có tài khoản <a href="{{ route('get.login')  }}" class="text-success">Đăng nhập</a>
                   </div>
               </div>
            </div>
        </div>
    </div>
@stop
